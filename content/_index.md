---
title: "Bienvenue"
---

Située à Nakutakoin, sur l'embouchure de la Dumbéa et à seulement 20 minutes
de Nouméa, la plate-forme privée de l’APNC, la plus grande de Nouvelle Calédonie,
exclusivement dédiée à l’ULM, offre un environnement fabuleux entre mer et montagne,
pour apprendre à piloter, exercer sa passion du vol ultraléger et pique niquer
ensuite en famille à l’ombre au bord de la rivière.

N'hésitez pas à nous contacter, mieux à rencontrer pilotes et instructeurs qui se
feront une joie de vous faire découvrir leur passion du vol ULM.

##### Contacts

- E-mail du [secrétariat](mailto:apnc.secretariat@gmail.com)
- E-mail du [Président](mailto:president.apnc@gmail.com)
- [Téléphone](tel:+687-748151) : 74 81 51

##### Adresse

209 route de Nakutakoin<br>
98835 Dumbéa

[Ouvrir la carte](geo:-22.17392,166.43282?z=18)
